# Flow

1. Post to /api/auth/login
2. Save token from response
3. Use token as Authorization Bearer for all other api endpoints

## Supported authentication methods
* Google OAuth2

### Google OAuth2
After acquiring authorization code you simply post it to login endpoint:
```
POST /api/auth/login
{
    "authorization_code": "some_code"
}
```

#### React example
Install react-google-login, and use below configuration
```javascript
const handleLogin = async (response: GoogleLoginResponse | GoogleLoginResponseOffline) => {
    // here post to /api/auth/login
    // authorization code is present in response.code
}

<GoogleLogin
          clientId={"CLIENT_ID"}
          responseType={"code"}
          accessType={"offline"}
          onSuccess={handleLogin}
          onFailure={handleLogin}
          cookiePolicy={"single_host_origin"}
      />
```
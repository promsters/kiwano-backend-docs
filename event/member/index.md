# Event member
## Permissions
Every event member can access members data.

In order to update/delete you must have event.write permission.

### Listing event members
```
GET /api/events/{eventId}/members
```
Sorting by joinedAt descending (newest first)

### Getting single event member
```
GET /api/event_members/{id}
```

### Updating
```
PUT /api/event_members/{id}
```
Updatabale properties:
1) role
   1) must be assigned to this event

### Deleting
```
DELETE /api/event_members/{id}
```
Permissions:
1) event.write

Rules:
1) Cannot remove owner
2) Cannot remove self
# Accepting event member invite
Available only for user which is subject of invite.

```
PUT /api/event_member_invites/{id}/accept
```
# Event member invite
* [Accept invite](./accept.md)
* [Decline invite](./decline.md)
* [Unregistered user invite](./unregistered_users.md)
## Permissions
In order to access event member invites you must have event.invite permission.

### Creating event member invite
```
POST /api/events/{eventId}/invites
```
There are two types of invite:
* for existing user (u specify user id in "user" property)
* for new users (u specify user email in "userEmail" property)

Rules:
* specified member role must belong to selected event
* user specified in invite cannot already be event member
* user email specified in invite cannot be already registered user

### Listing event member invites
```
GET /api/events/{eventId}/invites
```

### Getting single event member
```
GET /api/event_member_invites/{id}
```
User on the invitation can also use this endpoint

### Deleting
```
DELETE /api/event_member_invites/{id}
```

Rules:
* Cannot delete if status is other than NEW
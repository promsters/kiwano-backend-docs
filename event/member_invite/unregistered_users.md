# Unregistered user event member invite

Unregistered users receive email message with special register link containing invite id.

Invite id should be passed to auth endpoint as follows:
```
POST /api/auth/login
{
    "authorization_code": "some_code",
    "invite_id": "id_here"
}
```

After correct registration and auth user can accept invite as usual.
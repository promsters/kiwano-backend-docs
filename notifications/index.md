# Notifications

Notifications are sorted by creation date in descending order (newest first)

If seenAt property is null it means user haven't seen it yet.

### Getting not seen notifications
``
GET /api/user/{userId}/notifications?not_null[seenAt]=0
``

### Getting all notifications
``
GET /api/user/{userId}/notifications
``

### Marking notification as seen
```
PUT /api/notifications/{id}
{
    "seenAt": "2022-04-24 11:24:55"
}
```
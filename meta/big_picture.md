### Purpose
This app is a backend service hosting REST API used to power up application frontend.

Application is used to manage mainly non-profit events with volunteers.
1. Meta
   * [Big picture](./meta/big_picture.md)
   * [Database structure](./meta/database_structure.md)
   * [Architecture](./meta/architecture.md)
2. Development
   * [Installation](./development/installation.md)
   * [Coding guideline](./development/coding.md)
   * [Testing guideline](./development/testing.md)
3. Consuming API
   1. [Swagger docs](https://kiwano-backend.herokuapp.com/api/docs)
   2. Auth
      * [Authentication flow](./auth/auth_flow.md)
   3. Event
      * [Basic](./event/index.md)
      * [Icon](./event/icon.md)
      * [Role](./event/role.md)
      * [Member](./event/member/index.md)
      * [MemberInvite](./event/member_invite/index.md)
      * [Shift](./event/shift/index.md)
      * [ShiftMember](./event/shift/member.md)
   4. MediaObject
      * [Basic](./media_object/index.md)
   5. Notifications
       * [Basic](./notifications/index.md)
   6. Users
      * [Search](./user/search.md)
### Kiwano backend

#### Setup docker environment
First copy docker-compose.yml.dist into docker-compose.yml, make sure core container is exposed on available port, then execute:
```
docker-compose up -d
```
To enter core container (the one with php running) run:
```
./docker-enter
```
You might need to add permissions to execute this file, try executing:
```
chown +x docker-enter
```

#### Setup project
Once in core container run:
```
composer install
```

```
php bin/console lexik:jwt:generate-keypair
```

```
php bin/console doctrine:migrations:migrate
```


### [>> full docs <<](./docs/index.md)
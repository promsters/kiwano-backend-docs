1. Framework-agnostic classes should be placed in src/Application
2. Follow [PSR](https://www.php-fig.org/psr/) recommendations (especially PSR-1, PSR-4, PSR-11, PSR-14)
3. Follow [Symfony coding standard](https://symfony.com/doc/current/contributing/code/standards.html)
## Integration testing

Every single endpoint should be tested using simulated requests.

Tests are placed in ``tests/Integration`` directory.

### Running tests
```vendor/bin/phpunit```

### Fixtures
Fixtures are loaded automatically before each test.

You can find fixtures in ``fixtures`` directory.